Libraries
=========

These libraries are used by the processor from this extension but can also be re-used in other extensions.

These libraries below are developed by our team:

.. toctree::
   :maxdepth: 1
   :glob:

   libs/*

There is also a serial library developed by `Lucidar. <https://lucidar.me/en/serialib/cross-plateform-rs232-serial-library>`_
