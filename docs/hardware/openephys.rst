Open Ephys
==========

Open Ephys interface is still in development.

There is two types of processor to come:

- a reader directly from the Open-Ephys hardware.
- a zmq reader to read in output of the Open-Ephys software by using the Zmq node.
