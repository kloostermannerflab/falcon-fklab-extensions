

Detector
========

.. toctree::
   :maxdepth: 1
   :glob:

   detector/*

signal generators, filters, converters
======================================

.. toctree::
   :maxdepth: 1
   :glob:

   signal_generators_filters_converters/*

Event
=====

.. toctree::
   :maxdepth: 1
   :glob:

   event/*

Hardware Sink
=============

.. toctree::
   :maxdepth: 1
   :glob:

   output/*

Serializers
===========

.. toctree::
   :maxdepth: 1
   :glob:

   serializers/*
