Connecting to hardware
======================

This extensions enable the use on Falcon of two data acquisition systems (Neuralynx and OpenEphys) and two kind of output communications (serial, digital).
The serial communication allow to connect an arduino while the digital communication allow to work with some advantech hardware.


.. toctree::
   :maxdepth: 1
   :glob:

   hardware/neuralynx
   hardware/openephys
   hardware/arduino