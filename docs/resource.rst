Resources
=========

Resources are automatically added in falcon at installation time. It is available with their corresponding URIs.
It can be found either in the _build folder inside the falcon-core repository (post-build time) or the share folder in the installation path.

.. toctree::
   :maxdepth: 2
   :glob:

   resource/graphs/graphs
   resource/filters/filters


Use Case
========

.. toctree::
   :maxdepth: 1
   :glob:

   examples/ripple_detection
   examples/graph
   examples/overview