LevelCrossingDetector
=====================

.. image:: ../../images/LevelCrossingDetector.png
   :align: center


.. datatemplate:yaml:: ../../../processors/levelcrossingdetector/doc.yaml
   :template: template_processor.tmpl