.. _processors:

Processors
==========

Built-in processors can be found in the `falcon-fklab-extensions <https://bitbucket.org/kloostermannerflab/falcon-fklab-extensions.git>`_

For each processor, a description is given of:

- what data streams it expects on the input ports
- what data streams are generated on the output ports
- the configuration options (used to initialize the processor)
- the shared states that are exposed

See how to `extends a processor <https://falcon-core.readthedocs.io/en/latest/extensions/extend_processor.html>`_ on Falcon for more information.

List of processors
..................

.. toctree::
   :maxdepth: 2
   :glob:

   processors_description/processors